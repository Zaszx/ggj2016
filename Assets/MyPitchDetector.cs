﻿using UnityEngine;
using System.Collections;
using PitchDetector;

public class MyPitchDetector : MonoBehaviour
{
    public int Pitch;

    private Detector pitchDetector;                     //Pitch detector object
    public string selectedDevice { get; private set; }  //Mic selected
    public int pitchTimeInterval = 100;                     //Millisecons needed to detect tone
    private float[] data;                                       //Sound samples data
    private int[] detectionsMade;                               //Detections buffer
    private int maxDetectionsAllowed = 50;              //Max buffer size
    private int minFreq, maxFreq;                       //Max and min frequencies window
	private float refValue = 0.1f; 						// RMS value for 0 dB
    public float minVolumeDB = -17f;                        //Min volume in bd needed to start detection
    private int detectionPointer = 0;                       //Current buffer pointer
    public int cumulativeDetections = 5; 				//Number of consecutive detections used to determine current note

    void Awake()
    {
        pitchDetector = new Detector();
        pitchDetector.setSampleRate(AudioSettings.outputSampleRate);
    }

    IEnumerator Start()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.Microphone);
        if (Application.HasUserAuthorization(UserAuthorization.Microphone))
        {
            selectedDevice = Microphone.devices[0].ToString();
            GetMicCaps();

            //Estimates bufer len, based on pitchTimeInterval value
            int bufferLen = (int)Mathf.Round(AudioSettings.outputSampleRate * pitchTimeInterval / 1000f);
            Debug.Log("Buffer len: " + bufferLen);
            data = new float[bufferLen];

            detectionsMade = new int[maxDetectionsAllowed]; //Allocates detection buffer

            setUptMic();
        }
        else
        {
        }
    }

    void setUptMic()
    {
        //GetComponent<AudioSource>().volume = 0f;
        GetComponent<AudioSource>().clip = null;
        GetComponent<AudioSource>().loop = true; // Set the AudioClip to loop
        GetComponent<AudioSource>().mute = false; // Mute the sound, we don't want the player to hear it
        StartMicrophone();
    }

    public void StartMicrophone()
    {
        Debug.Log("Setting up mic");
        GetComponent<AudioSource>().clip = Microphone.Start(selectedDevice, true, 10, maxFreq);//Starts recording
        while (!(Microphone.GetPosition(selectedDevice) > 0)) { } // Wait until the recording has started
        GetComponent<AudioSource>().Play(); // Play the audio source!
        Debug.Log("Setted");
    }

    public void GetMicCaps()
    {
        Microphone.GetDeviceCaps(selectedDevice, out minFreq, out maxFreq);//Gets the frequency of the device
        if ((minFreq + maxFreq) == 0)
            maxFreq = 44100;
    }

    void Update()
    {
        GetComponent<AudioSource>().GetOutputData(data, 0);
        float sum = 0f;
        for (int i = 0; i < data.Length; i++)
            sum += data[i] * data[i];
        float rmsValue = Mathf.Sqrt(sum / data.Length);
        float dbValue = 20f * Mathf.Log10(rmsValue / refValue);
        if (dbValue < minVolumeDB)
        {
            return;
        }

        pitchDetector.DetectPitch(data);
        int midiant = pitchDetector.lastMidiNote();
        int midi = findMode();

        //Debug.Log(midi);

        Pitch = midi;

        detectionsMade[detectionPointer++] = midiant;
        detectionPointer %= cumulativeDetections;
    }

    public int findMode()
    {
        cumulativeDetections = (cumulativeDetections >= maxDetectionsAllowed) ? maxDetectionsAllowed : cumulativeDetections;
        int moda = 0;
        int veces = 0;
        for (int i = 0; i < cumulativeDetections; i++)
        {
            if (repetitions(i) > veces)
                moda = detectionsMade[i];
        }
        return moda;
    }
    int repetitions(int element)
    {
        int rep = 0;
        int tester = detectionsMade[element];
        for (int i = 0; i < cumulativeDetections; i++)
        {
            if (detectionsMade[i] == tester)
                rep++;
        }
        return rep;
    }


}
