﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class CalibrationManager : MonoBehaviour 
{
    public enum CalibrationStatus
    {
        notStarted,
        calibratingLowPitch,
        lowPitchDone,
        calibratingHightPitch,
        allDone,
        failed,

        count,
    }

    public CalibrationStatus status;
    public MicDataHolder micDataHolder;
    public MyPitchDetector pitchDetector;
    public static float lowPitchResult = 0;
    public static float highPitchResult = 0;
    public ParticleSystem graphParticleSystem;
    public Shaman shaman;

    public float accumulatedTime = 0;

    public string[] calibrationMessages = 
    {
        "Press the button to calibrate low pitch sound",
        "Low pitch sound calibration in progress...",
        "Press the button to calibrate high pitch sound",
        "High pitch sound calibration in progress...",
        "All Done! Press the button to start the game!",
        "Calibration failed. Press the button to retry.",
    };

    public string[] buttonTexts = 
    {
        "Start calibration",
        "",
        "Start calibration",
        "",
        "Start Game!",
        "Retry",
    };

	// Use this for initialization
	void Start () 
    {
        status = CalibrationStatus.notStarted;
        micDataHolder = new MicDataHolder(new AudioSource());
        //Thread.Sleep(10 * 1000);
        shaman.KeyDown();
	}

    public static float GetCalibrationValue()
    {
        if (lowPitchResult == 0 && highPitchResult == 0)
        {
            return 42;
        }
        float average = (lowPitchResult + highPitchResult) / 2.0f;
        return average;
    }

    void OnGUI()
    {
        GUIStyle centeredStyle = new GUIStyle();
        centeredStyle.alignment = TextAnchor.MiddleCenter;
        GUI.TextArea(new Rect(0, 20, Screen.width, 100), calibrationMessages[(int)status], centeredStyle);
        if (buttonTexts[(int)status].Length > 0)
        {
            if (GUI.Button(new Rect(Screen.width / 3.0f, 150, Screen.width / 6.0f, 100), buttonTexts[(int)status]))
            {
                switch (status)
                {
                    case CalibrationStatus.notStarted:
                        status = CalibrationStatus.calibratingLowPitch;
                        StartCoroutine(Calibrate(true));
                        break;
                    case CalibrationStatus.calibratingLowPitch:
                        break;
                    case CalibrationStatus.lowPitchDone:
                        status = CalibrationStatus.calibratingHightPitch;
                        StartCoroutine(Calibrate(false));
                        break;
                    case CalibrationStatus.calibratingHightPitch:
                        break;
                    case CalibrationStatus.allDone:
                        Application.LoadLevel("game");
                        break;
                    case CalibrationStatus.failed:
                        status = CalibrationStatus.notStarted;
                        break;
                    case CalibrationStatus.count:
                        break;
                    default:
                        break;
                }
            }
        }

    }
	
    IEnumerator Calibrate(bool low)
    {
        accumulatedTime = 0;
        float totalPitch = 0;
        int sampleCount = 0;
        while (accumulatedTime < 3.0f)
        {
            accumulatedTime = accumulatedTime + Time.deltaTime;
            float currentPitch = micDataHolder.Pitch;
            if (currentPitch != 0)
            {
                totalPitch = totalPitch + currentPitch;
                sampleCount++;
            }

            yield return new WaitForEndOfFrame();

        }

        if (low)
        {
            lowPitchResult = totalPitch / sampleCount;
            status = CalibrationStatus.lowPitchDone;
        }
        else
        {
            highPitchResult = totalPitch / sampleCount;
            if (highPitchResult > lowPitchResult)
            {
                status = CalibrationStatus.allDone;
            }
            else
            {
                status = CalibrationStatus.failed;
            }
        }
    }

    private PitchLevel GetPitchLevel(float p)
    {
        var retVal = PitchLevel.Low;
        if (p > 42)
        {
            retVal = PitchLevel.High;
        }
        else if (p == 0)
        {
            retVal = PitchLevel.None;
        }
        return retVal;
    }

	// Update is called once per frame
	void Update () 
    {
        micDataHolder.Update(pitchDetector, graphParticleSystem);
        shaman.SetPitch(GetPitchLevel(micDataHolder.Pitch));
	}
}
