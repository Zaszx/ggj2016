﻿using UnityEngine;
using System.Collections;
using System;

public class Shaman : MonoBehaviour
{
    public Sprite LowSprite;
    public Sprite HighSprite;
    public Sprite IdleSprite;

    private SpriteRenderer _renderer;
    private bool _isWalking = true;
    private Animator _animator;
    private bool _isDead;

    void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        _animator.SetBool("Walking", true);
    }

    public void KeyDown()
    {
        if (!_isDead)
        {
            _isWalking = false;
            _animator.SetBool("Walking", false);
        }
    }

    public void KeyUp()
    {
        if (!_isDead)
        {
            _isWalking = true;
            _animator.SetBool("Walking", true);
        }
    }

    public void SetPitch(PitchLevel level)
    {
        if (!_isWalking && !_isDead)
        {
            _animator.SetInteger("Pitch", (int)level);
            Debug.Log("setting sprite : " + level);
            //switch (level)
            //{
            //    case PitchLevel.Low:
            //        _renderer.sprite = LowSprite;
            //        break;
            //    case PitchLevel.High:
            //        _renderer.sprite = HighSprite;
            //        break;
            //    case PitchLevel.None:
            //        _renderer.sprite = IdleSprite;
            //        break;
            //    default:
            //        break;
            //}
            Debug.Log("setting sprite : " + level + " == " + _renderer.sprite);

        }
    }

    public void Die()
    {
        _isDead = true;
        _animator.SetBool("Walking", false);
    }
}
