﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour
{
    public Text MalText;
    public Button PlayAgainButton;
    public Image DimImage;

    void Start()
    {
        MalText.gameObject.SetActive(false);
        PlayAgainButton.onClick.AddListener(() =>
        {
            Application.LoadLevel("game");
        });
        PlayAgainButton.gameObject.SetActive(false);
        DimImage.gameObject.SetActive(false);
    }

    public void ShowDeathScreen()
    {
        MalText.gameObject.SetActive(true);
        PlayAgainButton.gameObject.SetActive(true);
        DimImage.gameObject.SetActive(true);
    }
}
