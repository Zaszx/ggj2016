﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;

public delegate void PlayerDeathHanlder();

public enum Color
{
    Green,
    Orange,
    Red
}

public class Player : MonoBehaviour
{
    public bool IsPaused
    {
        set
        {
            _animator.SetBool("Paused", value);
        }
    }

    [SerializeField]
    private ParticleSystem _particles;

    public PlayerDeathHanlder OnPlayerDeath;
    public float JumpForce;
    public float DeathMass;
    public Color Color;

    public bool _isAlive;
    private Rigidbody2D _thisRigidbody;
    private Animator _animator;
    private int _jumpTrigParam;
    private int _landTrigParam;
    private int _deathTrigParam;
    public bool _isActivePlayer;

    float timer = 0f;
    float duration = 0.6f;

	void Start()
    {
        _thisRigidbody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();

        _animator.SetInteger("Color", (int)Color);

        _jumpTrigParam = Animator.StringToHash("Jump");
        _landTrigParam = Animator.StringToHash("Land");
        _deathTrigParam = Animator.StringToHash("Death");

        _isActivePlayer = false;
        _particles.gameObject.SetActive(false);

        _isAlive = true;
    }
	
    void Update()
    {
        if (World.ISPAUSED)
        {
            return;
        }
        timer += Time.deltaTime;
        if (timer > duration)
        {
            GetComponent<AudioSource>().Play();
            timer = 0;
        }

        if (_isActivePlayer)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }
            if (Input.GetKeyDown(KeyCode.F))
            {
                StartCoroutine(Fireball());
            }
            if (Input.GetKeyDown(KeyCode.L))
            {
                StartCoroutine(Illuminate());
            }
            if (Input.GetKeyDown(KeyCode.G))
            {
                StartCoroutine(Fly());
            }
            if (Input.GetKeyDown(KeyCode.P))
            {
                StartCoroutine(Phase());
            }
        }
    }

    public IEnumerator Fly()
    {
        float oldGravityScale = _thisRigidbody.gravityScale;
        _thisRigidbody.gravityScale = 0;
        transform.position = new Vector3(transform.position.x, transform.position.y + 5.0f, transform.position.z);
        _particles.gameObject.SetActive(true);

        yield return new WaitForSeconds(5f);
        _thisRigidbody.gravityScale = oldGravityScale;
        _particles.gameObject.SetActive(false);
    }

    public IEnumerator Illuminate()
    {
        Light light = this.gameObject.AddComponent<Light>();
        light.intensity = 10000;
        yield return new WaitForSeconds(5);
        Destroy(light);
    }

    public IEnumerator Phase()
    {
        float oldGravityScale = this.GetComponent<Rigidbody2D>().gravityScale;
        this.GetComponent<Rigidbody2D>().gravityScale = 0;
        this.GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(5);
        this.GetComponent<BoxCollider2D>().enabled = true;
        this.GetComponent<Rigidbody2D>().gravityScale = oldGravityScale;
    }

    public IEnumerator Fireball()
    {
        GameObject fireballObject = Instantiate(Resources.Load<GameObject>("Prefabs/Fireball"));
        fireballObject.transform.position = this.transform.position;
        fireballObject.transform.position += new Vector3(3, 0, 0);
        fireballObject.AddComponent<Rigidbody2D>();
        fireballObject.AddComponent<BoxCollider2D>();
        fireballObject.GetComponent<Rigidbody2D>().velocity = new Vector3(10, 0, 0);
        fireballObject.GetComponent<Rigidbody2D>().gravityScale = 0;

        var thisLayer = LayerMask.LayerToName(gameObject.layer).Last();
        fireballObject.layer = LayerMask.NameToLayer("Bullet" + thisLayer);
        yield return new WaitForSeconds(5);
        if (fireballObject)
        {
            Destroy(fireballObject);
        }
    }

    public void ApplyPattern(MicPattern pattern)
    {
        if (!_isAlive)
        {
            return;
        }

        if (pattern == MicPattern.Jump)
        {
            Jump();
        }

        if (pattern == MicPattern.Attack)
        {
            StartCoroutine(Fireball());
        }

        if (pattern == MicPattern.Fly)
        {
            StartCoroutine(Fly());
        }
    }

    public void Jump()
    {
        _animator.SetTrigger(_jumpTrigParam);
        _animator.ResetTrigger(_landTrigParam);
        _thisRigidbody.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.collider.GetComponent<Obstacle>() != null)
        {
            //_thisRigidbody.gravityScale = 0;
            //_thisRigidbody.constraints = RigidbodyConstraints2D.FreezePositionY;
            GetComponent<BoxCollider2D>().size /= 4f;
            transform.SetParent(col.transform);
            _animator.SetTrigger(_deathTrigParam);
            _isAlive = false;

            if (OnPlayerDeath != null)
            {
                OnPlayerDeath();
            }
        }
        if (col.gameObject.tag == "Ground")
        {
            _animator.SetTrigger(_landTrigParam);
        }
    }

}
