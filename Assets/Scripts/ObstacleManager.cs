﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObstacleManager : MonoBehaviour
{
    public bool IsPaused
    {
        set
        {
            _obstacleSpeed = value ? 0.0001f : 0.1f;
        }
    }

    [SerializeField]
    private GameObject _wallPrefab;
    [SerializeField]
    private GameObject _spikePrefab;
    [SerializeField]
    private GameObject _barricadePrefab;
    [SerializeField]
    private Transform _spawnPoint;

    [SerializeField]
    private string _obstacleLayer;

    private float _timer = 0f;
    private float _spawnDuration = 3f;
    private float _obstacleSpeed = 0.1f;

    private List<GameObject> _obstacles = new List<GameObject>();

	void Start ()
    {
        _spawnDuration = Random.Range(0.5f, 15.0f);
	}
	
	void Update ()
    {
        if(World.ISPAUSED)
        {
            return;
        }
        _timer += Time.deltaTime;
        if (_timer > _spawnDuration)
        {
            _timer = 0f;
            _spawnDuration = 15f + Random.Range(-3.0f, 5.0f);
            float randomValue = Random.value;
            if(randomValue < 0.3f)
            {
                SpawnObstacle(_wallPrefab);
            }
            else if(randomValue < 0.6f)
            {
                SpawnObstacle(_spikePrefab);
            }
            else
            {
                SpawnObstacle(_barricadePrefab);
            }
            //SpawnObstacle(_wallPrefab);
        }

        _obstacles.RemoveAll(x => x == null);

        foreach (var obsGo in _obstacles)
        {
            obsGo.transform.Translate(Vector2.left * _obstacleSpeed);
        }
    }

    private void SpawnObstacle(GameObject obsPrefab)
    {
        var obsGo = (GameObject)Instantiate(obsPrefab, _spawnPoint.position, Quaternion.identity);
        obsGo.layer = LayerMask.NameToLayer(_obstacleLayer);
        obsGo.transform.SetParent(transform);
        _obstacles.Add(obsGo);
    }
}