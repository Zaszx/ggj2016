﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (World.ISPAUSED)
        {
            return;
        }
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.CompareTag("Fireball"))
        {
            Destroy(col.gameObject);
            Destroy(this.gameObject);
        }
    }
}
