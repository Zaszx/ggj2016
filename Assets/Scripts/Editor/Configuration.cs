﻿/*
With the native integration we don’t have a menu to setup options anymore. That’s because the majority of VSTU options are now
directly available within Unity project settings.
But this is not the case for the console redirection.
You should do something like this with an Editor script to properly setup what you want: (only once, this setting is global).
*/

using UnityEngine;
using SyntaxTree.VisualStudio.Unity.Bridge.Configuration;
using UnityEditor;

public class Configuration : MonoBehaviour
{

    [MenuItem("VSTU/Enable or Disable SendConsoleToVisualStudio")]
    static void SwitchSendConsoleToVisualStudio()
    {
        var cfg = Configurations.Active;
        var status = (cfg.SendConsoleToVisualStudio = !cfg.SendConsoleToVisualStudio) ? "enabled" : "disabled";

        EditorUtility.DisplayDialog("VSTU", "SendConsoleToVisualStudio is now " + status, "OK", "");
    }

}