﻿using UnityEngine;
using System.Collections;
using System.Threading;
using System.Collections.Generic;

public class World : MonoBehaviour
{
    [SerializeField]
    private UiManager _uiManager;
    [SerializeField]
    private Player[] _players;
    [SerializeField]
    private Shaman _shaman;

    [SerializeField]
    private GameObject _lightningPrefab;
    [SerializeField]
    private GameObject _explosionPrefab;

    private MicDataHolder _micDataHolder;
    private MicPatternMatcher _micPatternMatcher;
    private Player _activePlayer;
    private ObstacleManager[] _obstacleManagers;
    private BackgroundScroller[] _backgroundScrollers;

    private bool _isRecording;
    private float _timer;
    private float _duration = 0.6f;
    private List<PitchLevel> _recordedPitchs = new List<PitchLevel>();
    private int _playerCount = 3;

    public static bool ISPAUSED = false;

    public ParticleSystem graphParticleSystem;

    public int frameCount = 0;

    void Start ()
    {
        ISPAUSED = false;
        _micDataHolder = new MicDataHolder(GetComponent<AudioSource>());
        _micPatternMatcher = new MicPatternMatcher();
        _obstacleManagers = FindObjectsOfType<ObstacleManager>();
        _backgroundScrollers = FindObjectsOfType<BackgroundScroller>();

        foreach (var player in _players)
        {
            var player1 = player;
            player1.OnPlayerDeath += () =>
            {
                Instantiate(_lightningPrefab, player1.transform.position, Quaternion.Euler(270, 0, 0));
                Instantiate(_explosionPrefab, player1.transform.position, Quaternion.identity);

                bool anyPlayerAlive = false;
                foreach (Player p in _players)
                {
                    if (p._isAlive)
                    {
                        anyPlayerAlive = true;
                    }
                }
                --_playerCount;
                if (anyPlayerAlive == false)
                {
                    _shaman.Die();
                    StartCoroutine(ScreenShake(0.5f));
                    _uiManager.ShowDeathScreen();
                    ISPAUSED = true;
                }
                else
                {
                    StartCoroutine(ScreenShake(0.25f));
                }
            };
        }
    }
	
    IEnumerator ScreenShake(float duration)
    {
        var oldPos = Camera.main.transform.position;
        var timer = 0f;
        for (float i = 0; i < duration; i += Time.deltaTime)
        {
            Camera.main.transform.position = oldPos + Random.onUnitSphere * 3;
            yield return null;
        }
        Camera.main.transform.position = oldPos;
    }

    void Update()
    {
        if (ISPAUSED)
        {
            return;
        }
        var pitchLevel = _micDataHolder.Update(GetComponent<MyPitchDetector>(), graphParticleSystem);

        _shaman.SetPitch(pitchLevel);
        _micPatternMatcher.Update();

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            KeyDown(_players[0]);
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            
            KeyDown(_players[1]);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            KeyDown(_players[2]);
        }

        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            Keyup();
        }
        if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            Keyup();
        }
        if (Input.GetKeyUp(KeyCode.Alpha3))
        {
            Keyup();
        }

        foreach (Player p in _players)
        {
            p._isActivePlayer = false;
        }
        if (_activePlayer)
        {
            _activePlayer._isActivePlayer = true;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            _recordedPitchs.Add(PitchLevel.High);
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            _recordedPitchs.Add(PitchLevel.Low);
        }

        if (_isRecording)
        {
            _timer += Time.deltaTime;
            if (_timer > _duration)
            {
                var secondPitch = _micDataHolder.GetSecondsPitch(_timer);
                if (secondPitch != PitchLevel.None)
                {
                    _recordedPitchs.Add(secondPitch);
                }
                _timer = 0;
            }
        }

        if (frameCount == 0)
        {
            _players[0].GetComponent<Animator>().enabled = true;
        }
        if (frameCount == 1)
        {
            _players[1].GetComponent<Animator>().enabled = true;
        }
        if (frameCount == 2)
        {
            _players[2].GetComponent<Animator>().enabled = true;
        }
        frameCount++;
    }

    private void KeyDown(Player pl)
    {
        if (_activePlayer == null)
        {
            _isRecording = true;
            _activePlayer = pl;
            _micDataHolder.Clear();
        }

        foreach (var obs in _obstacleManagers)
        {
            obs.IsPaused = true;
        }
        foreach (var bg in _backgroundScrollers)
        {
            bg.IsPaused = true;
        }
        foreach (var p in _players)
        {
            p.IsPaused = true;
        }

        _shaman.KeyDown();
    }

    private void Keyup()
    {
        _isRecording = false;
        var p = _micPatternMatcher.RecognizePattern(_recordedPitchs);
        if (p != MicPattern.Invalid && _activePlayer)
        {
            _activePlayer.ApplyPattern(p);
        }
        _micDataHolder.Clear();
        _recordedPitchs.Clear();
        _timer = 0;
        _activePlayer = null;
        _shaman.KeyUp();

        foreach (var obs in _obstacleManagers)
        {
            obs.IsPaused = false;
        }
        foreach (var bg in _backgroundScrollers)
        {
            bg.IsPaused = false;
        }
        foreach (var pl in _players)
        {
            pl.IsPaused = false;
        }

    }

}
