﻿using UnityEngine;
using System.Collections;

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class BackgroundScroller : MonoBehaviour
{
    public bool IsPaused
    {
        get;set;
    }

    public int Order;
    public float ScrollingSpeed;
    public Transform[] Heads;
    public Transform[] Images;

    private float _targetOffset = -100f;


    void Start()
    {
        foreach (var r in GetComponentsInChildren<SpriteRenderer>())
        {
            r.sortingOrder = Order;
        }

    }

    void Update()
    {
        foreach (var img in Images)
        {
            if (!IsPaused)
            {
                img.Translate(Vector3.left * ScrollingSpeed);
            }

            if (img.position.x < _targetOffset)
            {
                var rightHeadX = Heads.Max(x => x.position.x);
                img.position = new Vector3(rightHeadX + this.GetComponentInChildren<SpriteRenderer>().bounds.size.x / 2.0f, img.position.y, img.position.z);
            }
        }
    }
}