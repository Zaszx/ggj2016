﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class DummyScript : MonoBehaviour 
{
    public enum RecordStatus
    {
        notStarted,
        started,
        done,
    }

    public AudioSource source = null;
    public RecordStatus recordStatus;
	void Start () 
    {
        source = GetComponent<AudioSource>();
	}

    public Dictionary<float, float> timeToPitchMap = new Dictionary<float, float>();
    public float totalTime = 0;

    public List<float> pitchValues = new List<float>();

    AudioClip BeginRecording()
    {
        int min = 0, max = 0;
        recordStatus = RecordStatus.started;
        string micName = Microphone.devices[0];
        Debug.Log(micName);

        Microphone.GetDeviceCaps(micName, out min, out max);

        Debug.Log(min + " " + max);
        AudioClip clip = Microphone.Start(micName, false, 10, 44100);
        return clip;
    }

    void OnGUI()
    {
        AnalyzeSound();
    }

    void EndRecording()
    {
        recordStatus = RecordStatus.done;
    }

     float refValue = 0.1f; // RMS value for 0 dB
     float threshold = 0.02f;      // minimum amplitude to extract pitch
     float rmsValue;   // sound level - RMS
     float dbValue;    // sound level - dB
     float pitchValue; // sound pitch - Hz
 
     void AnalyzeSound()
     {
         if (source.isPlaying == false)
         {
             return;
         }

         int numSamples = 1024;
         float[] samples = new float[numSamples];
         source.GetOutputData(samples, 0); // fill array with samples

         float[] spectrum = new float[numSamples];

         int i;
         float sum = 0;
         for (i=0; i < numSamples; i++)
         {
             sum += samples[i]*samples[i]; // sum squared samples
         }
         rmsValue = Mathf.Sqrt(sum/numSamples); // rms = square root of average
         dbValue = 20*Mathf.Log10(rmsValue/refValue); // calculate dB
         if (dbValue < -160) dbValue = -160; // clamp it to -160dB min
         // get sound spectrum
         source.GetSpectrumData(spectrum, 0, FFTWindow.BlackmanHarris);
         float maxV = 0;
         float maxN = 0;
         for (i=0; i < numSamples; i++)
         {
             if (spectrum[i] > maxV && spectrum[i] > threshold)
             {
                 maxV = spectrum[i];
                 maxN = i; // maxN is the index of max
             }
         }
         float freqN = maxN; // pass the index to a float variable
         if (maxN > 0 && maxN < numSamples-1)
         {
             var dL = spectrum[(int)maxN-1]/spectrum[(int)maxN];
             var dR = spectrum[(int)maxN+1]/spectrum[(int)maxN];
             freqN += 0.5f*(dR*dR - dL*dL);
         }
         float oldPitchValue = pitchValue;
         pitchValue = freqN * (AudioSettings.outputSampleRate / 2) / numSamples; // convert index to frequency
         if (pitchValue == 0) pitchValue = oldPitchValue;

         AddPitchValue(pitchValue);
         
         GUI.Label(new Rect(10, 10, 1000, 100), "Pitch: " + GetPitchInterpretation() + " " + "Decibels: " + dbValue);
    }

    void AddPitchValue(float pitch)
    {
        if (pitchValues.Count > 100)
        {
            pitchValues.RemoveAt(0);
        }
        pitchValues.Add(pitch);
    }

    float GetPitchInterpretation()
    {
        float pitch = 0;
        foreach (float data in pitchValues)
        {
            pitch = pitch + data;
        }
        pitch = pitch / pitchValues.Count;
        string result;
        if (pitch < 300)
        {
            result = "Low pitch";
        }
        else if (pitch < 600)
        {
            result = "Medium pitch";
        }
        else
        {
            result = "High Pitch";
        }
        return pitch;
    }

    void DrawPitchGraph()
    {
        foreach (KeyValuePair<float, float> data in timeToPitchMap)
        {
            
        }
    }

	void Update () 
    {
        if (recordStatus == RecordStatus.started)
        {
            totalTime = totalTime + Time.deltaTime;
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            if (recordStatus == RecordStatus.notStarted)
            {
                source.clip = BeginRecording();
                source.Play();
            }
            else if (recordStatus == RecordStatus.started)
            {
                EndRecording();
            }
        }
        if (recordStatus == RecordStatus.started && Microphone.IsRecording(Microphone.devices[0]) == false)
        {
            source.clip = BeginRecording();
            source.Play();
        }
        Debug.Log(source.isPlaying);
	}
}
