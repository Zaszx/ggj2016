﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public enum MicPattern
{
    Invalid,
    Jump,
    Attack,
    Fly,

    PatternCount,
}

public delegate void OnPatternSuccessHandler(MicPattern pattern);

public class PatternData
{
    public float seconds;
    public PitchLevel level;
}

public class MicPatternMatcher
{
    public event OnPatternSuccessHandler OnPatternSuccess;
    public Dictionary<MicPattern, List<PatternData>> patternData = new Dictionary<MicPattern, List<PatternData>>();
    public string[] patternNames = { "Invalid", "Jump", "Attack" };
    
    public float threshold = 0.1f;

    private List<PitchLevel> _jumpSequence = new List<PitchLevel>
    {
        PitchLevel.Low,
        PitchLevel.Low
    };

    private List<PitchLevel> _fireballSequence = new List<PitchLevel>
    {
        PitchLevel.Low,
        PitchLevel.High
    };

    private List<PitchLevel> _flySequence = new List<PitchLevel>
    {
        PitchLevel.High,
        PitchLevel.High
    };


    public MicPatternMatcher()
    {
        ReadPatterns();
    }

    public void Update()
    {
    }

    private bool DoesPatternHold(List<PitchLevel> commandSeq, List<PitchLevel> records)
    {
        if (records.Count < commandSeq.Count)
        {
            return false;
        }
        for (int i = 0; i < commandSeq.Count; i++)
        {
            if (records[i] != commandSeq[i])
            {
                return false;
            }
        }
        return true;
    }

    public MicPattern RecognizePattern(List<PitchLevel> _recordedPitchs)
    {
        if (DoesPatternHold(_jumpSequence, _recordedPitchs))
        {
            return MicPattern.Jump;
        }
        if (DoesPatternHold(_fireballSequence, _recordedPitchs))
        {
            return MicPattern.Attack;
        }
        if (DoesPatternHold(_flySequence, _recordedPitchs))
        {
            return MicPattern.Fly;
        }
        return MicPattern.Invalid;
    }

    float CalculateDistance(List<PatternData> pattern, List<MicFrameData> frameData)
    {
        float distance = 0;
        int startIndex = frameData.Count - 1;
        for (int patternIndex = pattern.Count - 1; patternIndex >= 0; patternIndex--)
        {
            float requiredTime = pattern[patternIndex].seconds;
            PitchLevel requiredLevel = pattern[patternIndex].level;

            float accumulatedTime = 0;

            for (int i = startIndex; i >= 0; i--)
            {
                if (frameData[i].Level != requiredLevel)
                {
                    distance = distance + frameData[i].Time;
                }
                accumulatedTime = accumulatedTime + frameData[i].Time;
                if (accumulatedTime >= requiredTime)
                {
                    startIndex = i - 1;
                    break;
                }
            }

            if (accumulatedTime < requiredTime)
            {
                return float.MaxValue;
            }
        }

        return distance;
    }

    void ReadPatterns()
    {
        string[] patternFiles = Directory.GetFiles("Patterns");
        foreach (string file in patternFiles)
        {
            string[] allLines = File.ReadAllLines(file);
            string patternName = allLines[0];
            int dataCount = int.Parse(allLines[1]);

            List<PatternData> micDataForThisPattern = new List<PatternData>();

            for (int i = 2; i < dataCount + 2; i++)
            {
                PatternData micData = new PatternData();
                string[] lineData = allLines[i].Split(' ');

                micData.seconds = float.Parse(lineData[0]);
                micData.level = (PitchLevel)int.Parse(lineData[1]);

                micDataForThisPattern.Add(micData);
            }

            patternData.Add(GetPatternFromName(patternName), micDataForThisPattern);
        }
    }

    MicPattern GetPatternFromName(string name)
    {
        for (int i = 0; i < patternNames.Length; i++)
        {
            if (patternNames[i] == name)
            {
                return (MicPattern)i;
            }
        }
        return MicPattern.Invalid;
    }
	
}
