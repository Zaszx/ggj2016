﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public enum PitchLevel
{
    Low,
    Medium,
    High,

    None,
}

public class MicFrameData
{
    public float Pitch { get; set; }
    public float Time { get; set; }
    public float Decibels { get; set; }
    public float Reserved1 { get; set; }
    public float Reserved2 { get; set; }
    public float Reserved3 { get; set; }
    public PitchLevel Level { get; set; }
}

public class MicDataHolder
{
    public float Pitch { get; private set; }

    private const float PitchCeiling = 75;
    private const int BufferSize = 500;
    private List<MicFrameData> _frameBuffer;
    private AudioSource _audioSource;
    private AudioClip _micInput;
    private float[] _samples;

    private Text testText;
    private Text pitchText;

    public MicDataHolder(AudioSource audioSource)
    {
        _audioSource = audioSource;
        _frameBuffer = new List<MicFrameData>(BufferSize);

        // Mic init
        var deviceName = "";
        int minFreq, maxFreq;
        Microphone.GetDeviceCaps(deviceName, out minFreq, out maxFreq);
        if (minFreq > 0) _micInput = Microphone.Start(deviceName, true, 1, minFreq);
        else _micInput = Microphone.Start(deviceName, true, 1, 44000);

        _samples = new float[_micInput.samples * _micInput.channels];
        GameObject testTextGo = GameObject.Find("TestText");
        if (testTextGo != null)
        {
            testText = GameObject.Find("TestText").GetComponent<Text>();
        }
        if (GameObject.Find("PitchText") != null)
        {
            pitchText = GameObject.Find("PitchText").GetComponent<Text>();
        }
    }

    public void Clear()
    {
        _frameBuffer.Clear();

    }

    public PitchLevel GetSecondsPitch(float time)
    {
        if(_frameBuffer.Count > 0)
        {
            //var highCount = 0;
            //var lowCount = 0;
            var nonZeroPitchSum = 0;
            var nonZeroPitchCount= 0;

            foreach (var data in _frameBuffer)
            {
                if (data.Pitch > 0)
                {
                    nonZeroPitchCount++;
                    nonZeroPitchSum += (int)data.Pitch;
                }

                //if (data.Level == PitchLevel.High)
                //{
                //    highCount++;
                //}
                //if (data.Level == PitchLevel.Low)
                //{
                //    lowCount++;
                //}
            }

            //return highCount > lowCount ? PitchLevel.High : PitchLevel.Low;

            var avgPitch = nonZeroPitchSum / nonZeroPitchCount;
            pitchText.text = avgPitch.ToString();
			if (testText != null)
			{
            	testText.text += " " + GetPitchLevel(avgPitch).ToString();// + ":" + highCount + "," + lowCount;
			}
            Clear();
            return GetPitchLevel(avgPitch);
            //return highCount > lowCount ? PitchLevel.High : PitchLevel.Low;
        }
        else
        {
            return PitchLevel.None;
        }
    }

    public PitchLevel Update(MyPitchDetector detector, ParticleSystem graphParticleSystem)
    {
        _micInput.GetData(_samples, 0);
        Pitch = detector.Pitch;
        //Debug.Log(Pitch);
        if (graphParticleSystem)
        {
            float clampedPitch = Mathf.Max(Pitch, 30);
            graphParticleSystem.transform.position = new Vector3(graphParticleSystem.transform.position.x, 14 + clampedPitch / 6.0f, graphParticleSystem.transform.position.z);
            graphParticleSystem.Emit(1);
        }
        if (GetPitchLevel(Pitch) != PitchLevel.None)
        {
            var data = new MicFrameData()
            {
                Level = GetPitchLevel(Pitch),
                Time = Time.deltaTime,
                Pitch = Pitch
            };

            //if (Input.GetKey(KeyCode.A))
            //{
            //    data.Level = PitchLevel.High;
            //}

            //if (Input.GetKey(KeyCode.Z))
            //{
            //    data.Level = PitchLevel.Low;
            //}

            _frameBuffer.Add(data);
        }

        if (_frameBuffer.Count > BufferSize)
        {
            _frameBuffer.RemoveAt(0);
        }

        return GetPitchLevel(Pitch);
    }

    private PitchLevel GetPitchLevel(float p)
    {
        var retVal = PitchLevel.Low;
        if (p > CalibrationManager.GetCalibrationValue())
        {
            retVal = PitchLevel.High;
        }
        else if (p == 0)
        {
            retVal = PitchLevel.None;
        }
        return retVal;
    }
}